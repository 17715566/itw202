import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Image style={styles.Image}
        source={{
          uri:'https://picsum.photos/100/100'
        }}
      />
      <Image style={styles.Images}

        source={require('./assets/r.png')}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Image:{
    height:100,
    width:100,
  },
  Images:{
    height:100,
    width:100,
  }
});
