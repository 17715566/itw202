import { StatusBar } from 'expo-status-bar';
import { ColorProptype, StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.text1}> 1 </Text>
      <Text style={styles.text2}> 2 </Text>
      <Text style={styles.text3}> 3 </Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'flex-start',
    paddingTop:80,
    paddingRight:100,
  },
  text1:{
    backgroundColor: 'red',
    paddingVertical:80,
    paddingHorizontal:30,
  },
  text2:{
    backgroundColor: 'blue',
    paddingVertical:80,
    paddingHorizontal:50,
  },
  text3:{
    backgroundColor: 'green',
    paddingVertical:80,
  },
});
