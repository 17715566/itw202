import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  const name = 'Tandin Wangchuk';
  return (
    
    <View style={styles.container}>
      <Text style={{fontSize:45}}> Getting started with react native!!!</Text>
      <Text style={{fontSize:20}}> My name is {name}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
