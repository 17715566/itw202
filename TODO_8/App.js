import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {

  const name = <Text style={styles.t1}> quick brown fox</Text>;
  return (
    <View style={styles.container}>
      <Text style={styles.t2}> The {name} jumps over the lazy dog</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  t1:{
    color:'black',
    fontWeight:'bold',
    fontSize: 30,
  },
  t2:{
    fontSize: 30.
  }
});
