import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Courses from './components/funcComponent';
export default class App extends React.Component{

  render = () => (

    <View style= {styles.container}>

      <Text> Practical 2</Text>
      <Text> Stateless and Stateful Components</Text>
      <Text style={styles.text} > You are ready to start the journey</Text>
       <Courses></Courses>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop:'10%',
  },
  text:{
    paddingTop:'5%',
  }
});
