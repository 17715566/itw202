import { View, Text,Image} from 'react-native'
import React from 'react'
import Header from '../components/Header'
import Button from '../components/Button'
import Background from '../components/Background'
import { logoutUser } from '../api/auth-api'

export default function HomeScreen() {
  return (
    <Background>
        <Header> Hi! Welcome to Home Screen.</Header>
        <Image style={{width:100, height:100, borderRadius:20}}
        source={require('../../assets/home.png')}
        />
        <Button 
        mode='contained'
        onPress={()=>{
          logoutUser();
        }}>
          Logout</Button>
    </Background>
  )
}