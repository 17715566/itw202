import react from 'react'
import { ActivityIndicator } from 'react-native-paper'
import Background from '../components/Background'
import firebase from 'firebase/compat'

export default function AuthLoadingScreen({navigation}){
    firebase.auth().onAuthStateChanged((user)=>{
        if(user){
            //user login
            navigation.reset({
                routes:[{name: 'HomeScreen'}],
            });
        }
        else{
            //user not login
            navigation.reset({
                routes:[{name: 'StartScreen'}],
            });

        }
    })
    return(
        <Background>
            <ActivityIndicator size='large'/>
        </Background>
    )
}