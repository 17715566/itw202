import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import PlacesList from '../Component/Places/PlacesList'

const AllPlaces = () => {
  return (
    <PlacesList/>
  )
}

export default AllPlaces

const styles = StyleSheet.create({})