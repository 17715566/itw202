import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {NavigationContainer} from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AllPlaces from './Screens/AllPlaces';
import AddPlace from './Screens/AddPlace';
import { IconButton } from 'react-native-paper';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <>
      <StatusBar style='dark'/>
      <NavigationContainer>
        <Stack.Navigator>
          < Stack.Screen 
          name="AllPlaces" 
          component={AllPlaces}
          options={({ navigation }) => ({
            title: 'Your Favourite Places',
            headerRight: ({ tintColor }) => (
              <IconButton
                icon="add"
                size={24}
                color={tintColor}
                onPress={() => navigation.navigate('AddPlaces')}
              />
            ),
          })}
          />
          < Stack.Screen name="AddPlace" component={AddPlace}/>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
