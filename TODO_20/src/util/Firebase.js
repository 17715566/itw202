import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyDx-eP4k0MZgHsRE5o4sLL9FcvZb8gTuoU",
  authDomain: "fir-operation-b28f1.firebaseapp.com",
  databaseURL: "https://fir-operation-b28f1-default-rtdb.firebaseio.com",
  projectId: "fir-operation-b28f1",
  storageBucket: "fir-operation-b28f1.appspot.com",
  messagingSenderId: "515755054018",
  appId: "1:515755054018:web:590ce3abdca30a79757c5b"
}
firebase.initializeApp(CONFIG)

export default firebase;
