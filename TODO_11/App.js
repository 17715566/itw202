import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button } from 'react-native';
import React, { useState} from 'react';

export default function App() {
  const [count, setCount] = useState(1);
  const [text, setText] = useState("The button isn't press yet");
  const [able, setAble] = useState(false);
  return (
    <View style={styles.container}>
      <Text>{text}</Text>
      <View style={{backgroundColor:'blue', width:300}}>
        <Button title='Press Me'
        onPress={()=>{
                if(count<4){
                  setText("The button has been pressed "+count+" times!")
                  setCount(count+1)
                  if(count==3){
                    setAble(True);
                  }
                }
              }
            }
             disabled={able}/>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: 200,
  },
});
