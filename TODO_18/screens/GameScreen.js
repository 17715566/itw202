import { StyleSheet, Text, View, Alert , FlatList, content} from 'react-native'
import React, {useState, useEffect} from 'react'
import Title from '../component/Title';
import NumberContainer from '../component/game/NumberContainer';
import PrimaryButton from '../component/PrimaryButton';
import InstructionText from '../component/ui/InstructionText';
import Card from '../component/ui/Card';
import {Ionicons} from '@expo/vector-icons'
import GuessLogItem from '../component/game/GuessLogItem';
import { useWindowDimensions } from 'react-native';

function generateRandomBetween(min, max, exclude){
  const rndNum = Math.floor(Math.random() * (max-min)) + min;

  if (rndNum == exclude){
    return generateRandomBetween(min, max, exclude)
  }
  else{
    return rndNum;
  }
}
let minBoundry = 1;
let maxBoundry = 100;

const GameScreen = ({userNumber, onGameOver}) => {
  const initialGuess = generateRandomBetween(1, 100, userNumber)
  const[currentGuess, setCurrentGuess] = useState(initialGuess)
  const[guessRounds, setGuessRounds] = useState([initialGuess]);
  const{width, height} = useWindowDimensions();

  useEffect(() =>{
    if(currentGuess === userNumber){
      onGameOver(guessRounds.length);
    }
  },[currentGuess, userNumber, onGameOver])

  useEffect(() =>{
    minBoundry = 1;
    maxBoundry = 100;
  },[])
  
    function nextGuessHandler(direction){
      if (
        ( direction === 'lower' && currentGuess < userNumber) ||
        (direction === 'greater' && currentGuess > userNumber))
        {
          Alert.alert("Dont't lie!", 'You know that is wrong...',
          [{text: 'sorry', style: 'cancel'}
        ])
      
        return;
        }

      if(direction =='lower'){
        maxBoundry = currentGuess;
      }else{
        minBoundry = currentGuess +1 ;
      }
      console.log(minBoundry, maxBoundry)
      const newRndNumber = generateRandomBetween(minBoundry, maxBoundry, currentGuess)

      setCurrentGuess(newRndNumber)
      setGuessRounds((prevGuessRounds => [newRndNumber, ...prevGuessRounds]))
    }

    const guessRoundsListLength = guessRounds.length;

        let content =(
        <>    
          <NumberContainer>{currentGuess}</NumberContainer>
            <Card>
                <View style={styles.buttonsContainerWide}>
                  <View style={styles.buttonContainer}>
                    <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                      <Ionicons name='md-add' size={24} color= 'white'/>
                    </PrimaryButton>
                </View>

                  <NumberContainer>{currentGuess}</NumberContainer>
                <View style={styles.buttonContainer}>
                  <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                    <Ionicons name='md-remove' size={24} color='white'/>
                      </PrimaryButton>
                    </View>
                </View>
            </Card>
         </>
        );

        if (width > 500){
          content=(
            <>
            <View style={styles.buttonsContainerWide}>
                <View style={styles.buttonContainer}>
                  <PrimaryButton onPress={nextGuessHandler.bind(this, 'greater')}>
                    <Ionicons name='md-add' size={24} color= 'white'/>
                  </PrimaryButton>
              </View>

                <NumberContainer>{currentGuess}</NumberContainer>
              <View style={styles.buttonContainer}>
                <PrimaryButton onPress={nextGuessHandler.bind(this, 'lower')}>
                  <Ionicons name='md-remove' size={24} color='white'/>
                    </PrimaryButton>
                  </View>
              </View>
            </>
          )
        }
      
        return (
          <View style={styles.screen}>
            <Title>Opponent's Guess</Title>
            {content}
              <View style={styles.listContainer}>
                <FlatList
                  data={guessRounds}
                  renderItem={(itemData) =>
                    <GuessLogItem
                  roundNumber={guessRoundsListLength - itemData.index}
                  guess = {itemData.item}
                  />
                }
                  keyExtractor={(item) => item}
                />
              </View>  
          </View>
        )
      }

export default GameScreen

const styles = StyleSheet.create({
  buttonsContainerWide:{
    flexDirection: 'row',
    alignItems: 'center'
  },
  instructionText:{
    marginBottom: 12
  },
  screen:{
    flex: 1,
    padding: 12,
    alignItems: 'center',
  }, 
  text: {
    paddingLeft: 138
  },
  buttonsContainer:{
    flexDirection: 'row',
  },
  buttonContainer: {
    flex: 1,
  },
  listContainer:{
    flex: 1,
    padding: 12,
  }

})