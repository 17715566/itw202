import React,{useState} from 'react'
import {TextInput, Text, View, StyleSheet, Alert} from 'react-native'
import PrimaryButton from '../component/PrimaryButton'
import Title from '../component/Title';
import InstructionText from '../component/ui/InstructionText';
import Card from '../component/ui/Card';
import { Dimensions, KeyboardAvoidingView, useWindowDimensions } from 'react-native';
import { ScrollView } from 'react-native';

const StartGameScreen = ({onPickNumber}) =>{
  const [enteredNumber, setEnteredNumber] = useState('');

  const {width, height} = useWindowDimensions();

function numberInputHandler(enteredText){
  setEnteredNumber(enteredText);
}

function resetInputHandler(){
  setEnteredNumber('');
}

function confirmInputHandler(){
  const chosenNumber = parseInt(enteredNumber);

  if(isNaN(chosenNumber) || chosenNumber <=0 || chosenNumber > 99) {
    Alert.alert('InvalidNumber!', 'Number has to be a Number between 1 and 99.',
    [{text:'okay', style:'destructive', onPress: resetInputHandler}]
    )
    return;
  }
    // console.log(chosenNumber)
    onPickNumber(chosenNumber)
}

const marginTopDistance = height < 380 ? 30: 100;

  return (
    <ScrollView style={styles.screen}>
      <KeyboardAvoidingView style={styles.screen} behavior="position">
        <View style={[styles.rootContainer, {marginTop: marginTopDistance}]}>
          <Title> Guess My Number</Title>
          <Card style={styles.inputContainer}>
          <InstructionText> Enter a Number </InstructionText>
              <TextInput
              style={styles.numberInput}
              keyboardType='number-pad'
              maxLength={2}
              autoCapitalize='none'
              autoCorrect={false}
              value={enteredNumber}
              onChangeText={numberInputHandler}
              />
              <View style={styles.buttonsContainer}>
                <View style={styles.buttonContainer}>
                    <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
                </View>
                <View style={styles.buttonContainer}>
                    <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
                </View>
              </View>
          </Card>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  )
}

export default StartGameScreen
// const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  screen:{
    flex: 1,
  },

  rootContainer:{
    flex: 1,
    // marginTop: deviceHeight < 300 ? 30: 100,
    alignItems: 'center'
  },

  inputContainer: {
    justifyContent: 'center',
    alignItems:'center',
    marginTop: 100,
    marginHorizontal: 24,
    padding: 16,
    backgroundColor: '#72063C',
    borderRadius: 8,
    elevation: 4,
    shadowColor: 'black',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 6,
    shadowOpacity: 0.25
  },
  numberInput: {
    height: 50,
    width: 50,
    fontSize: 32,
    borderBottomColor:'#DDB52F',
    borderBottomWidth: 2,
    color: '#DDB52F',
    marginVertical: 8,
    fontWeight: 'bold',
    textAlign: 'center'
},
buttonsContainer: {
    flexDirection: 'row',
  
},
buttonContainer: {
    flex: 1,
    width: 100
}
})