import { StyleSheet,ImageBackground,SafeAreaView } from 'react-native';
import StartGameScreen from './screens/StartGameScreen';
import { LinearGradient } from 'expo-linear-gradient';
import React,{useState} from 'react';
import GameScreen from './screens/GameScreen';
import Colors from './constants/Color';
import GameOverScreen from './screens/GameOverScreen';
// import { useFonts } from 'expo-font';
import { StatusBar } from 'expo-status-bar';

export default function App() {
  const [userNumber, setUserNumber] = useState();
  const [gameIsOver, setGameIsOver] = useState(true);
  const [guessRounds, setGuessRounds] = useState(0);

    // const [fontsLoaded] = useFonts({
    //   'open-sans': require('./assets/Fonts/OpenSans-Regular.ttf'),
    //   'open-sans-bold': require('./assets/Fonts/OpenSans-Bold.ttf'),
    // })

      function pickedNumberHandler(pickerNumber){
        setUserNumber(pickerNumber);
        setGameIsOver(false);

      }
      function gameOverHandler(numberOfRounds){
        setGameIsOver(true);
        setGuessRounds(numberOfRounds)
      }
      function StartNewGameHandler(){
        setUserNumber(null);
        setGuessRounds(0);
      }
      let screen = <StartGameScreen onPickNumber = {pickedNumberHandler}/>

      if(userNumber){
        screen =<GameScreen userNumber={userNumber} onGameOver = {gameOverHandler}/>
      }
      if(gameIsOver && userNumber){
        screen = <GameOverScreen
                userNumber={userNumber}
                roundsNumber={guessRounds}
                onStartNewGame={StartNewGameHandler}
        />
}

  return (
    <>
      <StatusBar style='light'/>
        <LinearGradient 
          colors= {['#4e0329', '#ddb52f']} style={styles.container}> 

          <ImageBackground 
          
          source = {require('./assets/image/pie.png')} 
          resizeMode='cover' 
          style ={styles.image}
          imageStyle={styles.backgroundImage}> 

          <SafeAreaView style={styles.container1}>
          {screen}
          </SafeAreaView>
          </ImageBackground>

        </LinearGradient>
    </>
  );
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
  }, 
  container1: {
    flex: 1,
    paddingTop: 30,
  },
  image:{
    height: '100%',
    width: '100%',
    position: 'absolute'
  },
  backgroundImage:{
    opacity: 0.15,
  }
});
