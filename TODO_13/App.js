import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

function Screen1({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>First Screen</Text>
      <Button
        title="Go to second screen"
        onPress={() => navigation.navigate('SecondScreen')}
      />
    </View>
  );
}

function Screen2({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Second Screen</Text>
      <Button
        title="Go to third screen"
        onPress={() => navigation.navigate('ThirdScreen')}
      />
    </View>
  );
}

function Screen3({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Third Screen</Text>
      <Button
        title="Go Back"
        onPress={() => navigation.navigate('Go Back')}
      />
    </View>
  );
}

const Stack = createNativeStackNavigator();

const App=()=> {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Go Back">
        <Stack.Screen name="Go Back" component={Screen1} />
        <Stack.Screen name="SecondScreen" component={Screen2} />
        <Stack.Screen name="ThirdScreen" component={Screen3} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
