import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

export default function App() {
  const [name,setName] = useState('name');
  return (
    <View style={styles.container}>
      <Text>What is your name?</Text>
      <TextInput onChangeText={(val)=>setName(val)} />
      <Text>
        Hi {name} from Gyelpozhing College of Information Technology
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
