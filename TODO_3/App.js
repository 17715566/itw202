import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './component/nameExport'
import division from './component/defaultExport'; 
export default function App() {
  return (
    <View style={styles.container}>
      <Text> Mathetic Result </Text>
      <Text> Result of addition: {add(3, 4)}</Text>
      <Text> Result for multiplication: {multiply(2, 4)}</Text>
      <Text> Result of Division: {division(10, 2)}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
