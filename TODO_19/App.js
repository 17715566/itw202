import { StatusBar } from 'expo-status-bar';
import {NavigationContainer} from '@react-navigation/native'
import { StyleSheet, Text, View, } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AllPlaces from './Screens/AllPlaces';
import AddPlaces from './Screens/AddPlaces';
import IconButton from './components/UI/IconButton';
// import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Colors } from './constants/Colors';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
    <>
      <StatusBar style='dark'/>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerStyle: {backgroundColor: Colors.primary500},
            headerTintColor: Colors.gray700,
            contentStyle: {backgroundColor: Colors.gray700}
          }}
        >
          <Stack.Screen 
          name='AllPlaces' 
          component={AllPlaces}
          options={({ navigation }) => ({
            title: 'Your Favorite Places',
            headerRight: ({ tintColor }) => (
              <IconButton
              icon="add"
              size={24}
              color={tintColor}
              onPress={() => navigation.navigate('AddPlaces')}
              />
            ),
          })}
          />
          <Stack.Screen 
          name='AddPlaces' 
          component={AddPlaces}
          options= {{
            title: 'Add a new Place'
          }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
